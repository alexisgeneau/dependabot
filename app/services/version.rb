# frozen_string_literal: true

class Version
  VERSION = "0.27.5"
end
